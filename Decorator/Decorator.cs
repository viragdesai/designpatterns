﻿using System;

namespace Decorator
{
    public class Decorator
    {
        /// <summary>
        /// The 'Component' interface
        /// </summary>
        public interface IVehicle
        {
            string Make { get; }
            string Model { get; }
            double Price { get; }
        }

        public class HyundaiVerna : IVehicle
        {
            public string Make
            {
                get { return "HyundaiVerna"; }
            }

            public string Model
            {
                get { return "Petrol"; }
            }

            public double Price
            {
                get { return 1200000; }
            }
        }

        /// <summary>
        /// The 'ConcreteComponent' class
        /// </summary>
        public class HondaCity : IVehicle
        {
            public string Make
            {
                get { return "HondaCity"; }
            }

            public string Model
            {
                get { return "CNG"; }
            }

            public double Price
            {
                get { return 1000000; }
            }
        }

        /// <summary>
        /// The 'Decorator' abstract class
        /// </summary>
        public abstract class VehicleDecorator : IVehicle
        {
            private IVehicle _vehicle;

            public VehicleDecorator(IVehicle vehicle)
            {
                _vehicle = vehicle;
            }

            public string Make
            {
                get { return _vehicle.Make; }
            }

            public string Model
            {
                get { return _vehicle.Model; }
            }

            public double Price
            {
                get { return _vehicle.Price; }
            }

        }

        /// <summary>
        /// The 'ConcreteDecorator' class
        /// </summary>
        public class SpecialOffer : VehicleDecorator
        {
            public SpecialOffer(IVehicle vehicle) : base(vehicle) { }

            public int DiscountPercentage { get; set; }
            public string Offer { get; set; }

            public double Price
            {
                get
                {
                    double price = base.Price;
                    int percentage = 100 - DiscountPercentage;
                    return Math.Round((price * percentage) / 100, 2);
                }
            }

        }

        /// <summary>
        /// Decorator Pattern Demo
        /// </summary>
        class Program
        {
            static void Main(string[] args)
            {
                // Basic vehicle
                HondaCity car = new HondaCity();

                Console.WriteLine("Honda City base price are : {0}", car.Price);

                // Special offer
                SpecialOffer offer = new SpecialOffer(car);
                offer.DiscountPercentage = 25;
                offer.Offer = "25 % discount";

                Console.WriteLine("{1} @ Diwali Special Offer and price are : {0} ", offer.Price, offer.Offer);

                Console.WriteLine("-------------------------------------------------");

                HyundaiVerna hyundaiVerna = new HyundaiVerna();
                Console.WriteLine("Hyundai Verna base price are: {0}", hyundaiVerna.Price);

                SpecialOffer specialOffer = new SpecialOffer(hyundaiVerna);
                specialOffer.DiscountPercentage = 12;
                specialOffer.Offer = "12% discount";

                Console.WriteLine("{1} @ Diwali Special Offer and price are : {0} ", specialOffer.Price, specialOffer.Offer);


                Console.ReadKey();

            }
        }
    }
}
